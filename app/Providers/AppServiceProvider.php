<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        /*Validator::extend('photo_rules',function($attribute, $value, $params, $validator) {
            $data = strlen(base64_decode($value));
            $size = (strlen($data) * 3 / 4) - substr_count(substr($data, -2), '=');
            if ($size>1024){
                return "Image size are greater like ".$size;
            }

            return $size;
        });*/
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

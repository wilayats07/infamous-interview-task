<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function index()
    {
        $users=User::latest()->get();
        return $users;
    }
    public function store(Request $request){
        $this->validate($request, [
            'name'=>'required|min:5|max:30',
            "phone"=>"numeric|regex:/^[0-9][0-9]{10,14}$/",
            "email"=>"required|email|max:50",
            "gender"=>"required",
            "dob"=>"required|before:now|before:-18 years",
            "biography"=>"required|min:10|max:100",
            "image_url"=>"required"
        ]);
        $image_url = $request->image_url;
        $imagePath = "";
        try
        {
            if(strlen($image_url) > 128) {
                list($ext, $data)   = explode(';', $image_url);
                list(, $data)       = explode(',', $data);
                $data = base64_decode($data);
                $fileName = strtotime(date("Y-m-d H:i:s"))."_".mt_rand().'.jpg';
                file_put_contents('uploads/images/'.$fileName, $data);
                $imagePath = "uploads/images/".$fileName;
            }
        }
        catch (\Exception $e) {
            $msg = $e;
        }
        $data=[
            'name'=>$request->name,
            'phone'=>$request->phone,
            'email'=>$request->email,
            'dob'=>date("Y-m-d",strtotime($request->dob)),
            'biography'=>$request->biography,
            "image_url"=>$imagePath,
            "password"=>bcrypt("0")
        ];
        return response()->json(User::create($data));

    }
}

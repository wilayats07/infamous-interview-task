<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [
                'name'=>'required|min:5|max:30',
                "phone"=>"numeric|min:11",
                "email"=>"required|email|max:50",
                "gender"=>"required",
                "dob"=>"required|before:now|before:-18 years",
                "biography"=>"required|min:10|max:100",
                "image_url"=>"required|size:1024"
            ]
        ];
    }
}

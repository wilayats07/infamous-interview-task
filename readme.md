
## Installation of Infamous Interview Task
- clone the whole repository to local server
- Then move this directory on terminal and run the following command ``composer install``
- After installation Laravel dependency run the following command ``php artisan  key:generate``
- Database Connection in ```.env file ===> DB_CONNECTION=mysql
                                         DB_HOST=127.0.0.1
                                         DB_PORT=3306
                                         DB_DATABASE=task
                                         DB_USERNAME=root
                                         DB_PASSWORD=
                                         ```
 change it your requirement and run the following command `` php artisan migrate``
 - Now run the following command ``npm install`` it will install all nodejs dependency for webpack.
 - and then run the following command ``npm run watch or npm run production``
- At last run the following command ``php artisan serve``will run the project on server  ``http://localhost:8000``
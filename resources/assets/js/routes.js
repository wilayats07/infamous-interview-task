
import VueRouter from 'vue-router';
var routes =[
    {
        path: '/',
        name:"Home",
        component: require('./pages/Home.vue')
    },
    {
        path: '/users',
        name:"Users",
        component: require('./pages/ListUsers.vue')
    },
];
export default new VueRouter({
    routes,
    linkActiveClass: 'is-active'
});
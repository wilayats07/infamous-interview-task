<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta id="token" name="token" value="{{ csrf_token() }}">
        <title>Task in Vuejs</title>
        <link rel="stylesheet" href="/css/app.css">
    </head>
    <body>
    <div id="app">

        <div class="container">
            <div class="col-md-1"></div>
            <div class="col-md-10 marginTop20px">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li>
                                <router-link to="/">Home</router-link>
                            </li>
                            <li>
                                <router-link  to="/users">Users Lists</router-link>
                            </li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <Router-view></Router-view>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="/js/app.js"></script>
    </body>
</html>
